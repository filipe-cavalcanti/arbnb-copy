const locations = [
    {
        id: 1,
        nome: 'Recife'
    },
    {
        id: 2,
        nome: 'João Pessoas'
    },
    {
        id: 3,
        nome: 'Campina Grande'
    },
    {
        id: 4,
        nome: 'Rio de Janeiro'
    },
];
let rooms = [];
const createRoomCard = (roomModel, totalPrice) => {
    return `
    <div class="room">
            <div class="room-header">
                <img class="room-img" width="100%" src="${roomModel.photo}" alt="room">
            </div>
            <div class="room-type">
                <div>
                    ${roomModel.propertyType}
                </div>
            </div>
            <div class="room-text">
                <div>
                    ${roomModel.name}
                </div>
                <div>
                    <strong>R$ ${roomModel.price}</strong>/noite
                </div>
                <div>
                    ${totalPrice ? `<small id="checkout-help" class="form-text text-muted">Total de R$ ${totalPrice}</small>` : ''}
                </div>
            </div>
        </div>
    `
};

const calculateDiffDays = () => {
    const checkIn = new Date($('#check-in').val());
    const checkout = new Date($('#checkout').val());
    const timeDiff = Math.abs(checkIn.getTime() - checkout.getTime());
    return Math.ceil(timeDiff / (1000 * 3600 * 24));
};

const queryRoom = () => {
    const diffDay = calculateDiffDays();
    const roomContainer = $('#room-container');
    const location = $('#location-select').val();
    console.log(location);
    roomContainer.empty();
    $.ajax({
        url: 'https://v2-api.sheety.co/ec3383d16c2b955334e63705b1ab319d/roomsHiringCoders/convertcsv',
        success: result => {
            const resultFiltred = result.convertcsv.filter(item => item.location === parseInt(location, 10));
            rooms = resultFiltred;
            resultFiltred.forEach(item => roomContainer.append(createRoomCard(item, calculateTotalPrice(diffDay, item.price))));
            showInfo();
        }
    });
};

const calculateTotalPrice = (amountDay, dayPrice) => amountDay * dayPrice;

const selectLocation = () => {
    const locationSelect = $('#location-select');
    locationSelect.append(...locations.map(item => `<option value="${item.id}">${item.nome}</option>`));
};

const showInfo = () => {
    const locationSelect = parseInt($('#location-select').val(), 10);
    const locationSelectObj = locations.find(item => item.id === locationSelect);
    const infoContainer = $('#info');
    infoContainer.empty();
    infoContainer.append(`
        <h6>${rooms.length} acomodações</h6>
        <h1><strong>Acomodações em ${locationSelectObj.nome}</strong></h1>
    `)

}

selectLocation();
